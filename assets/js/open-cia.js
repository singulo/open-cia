/*
* Ajustes no layout a pedido do cliente
*/

/**
 * Pesquisa não precisa ter os campos de busca finalidade e banheiros
 */
$('.pesquisa-avancada-1 .form-filtro label[for=finalidade]').closest('div.col-md-2').remove();
$('.pesquisa-avancada-1 .form-filtro label[for=banheiros]').closest('div.col-md-1').remove();
$('.pesquisa-avancada-1 .form-filtro label[for=dormitorios]').closest('div.col-md-1').addClass('col-md-2').removeClass('col-md-1');
$('.pesquisa-avancada-1 .form-filtro label[for=suites]').closest('div.col-md-1').addClass('col-md-2').removeClass('col-md-1');
$('.pesquisa-avancada-1 .form-filtro label[for=garagem]').closest('div.col-md-1').addClass('col-md-2').removeClass('col-md-1');

$($($('.menu-items').find('li')[2]).find('a')).text('CONDOMÍNIOS');
$($($('.menu-mobile-1').find('li')[2]).find('a')).text('CONDOMÍNIOS');


$('.form-pesquisa-banner-1').find('.filtro-finalidade').remove();
$('.form-pesquisa-banner-1 select[name=id_tipo]').closest('div.col-md-6').addClass('col-md-12').removeClass('col-md-6');
$('.form-pesquisa-banner-1 select[name=dormitorios]').closest('div.col-md-4').addClass('col-md-6').removeClass('col-md-4');
$('.form-pesquisa-banner-1 select[name=garagem]').closest('.col-md-4').remove();
$('.form-pesquisa-banner-1 select[name=suites]').closest('.col-md-4').remove();

$('.form-pesquisa-banner-1 .form-filtro h3').text('');

function criarLabelParaPesquisaNoBanner(elem, forName, labelText)
{
    var label = document.createElement("Label");
    label.setAttribute("for", forName);
    label.innerHTML = labelText;

    elem.prepend(label);
}

criarLabelParaPesquisaNoBanner($('.form-pesquisa-banner-1 select[name=id_tipo]').closest('div.col-md-12'), 'id_tipo', 'O que você procura?');
criarLabelParaPesquisaNoBanner($('.form-pesquisa-banner-1 select[name=cidade]').closest('div.col-md-6'), 'cidade', 'Em qual cidade?');
criarLabelParaPesquisaNoBanner($('.form-pesquisa-banner-1 select[name=dormitorios]').closest('div.col-md-6'), 'dormitorios', 'Quantos dormitórios precisa?');
