<? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>

<!doctype html>
<html lang="pt-BR">

<head>

    <title><?= $_SESSION['filial']['nome']; ?></title>
    <meta name="discription" content=""> <!--- Discrição do site !-->
    <meta name="keywords" content=""> <!--- palavras chave !-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <link rel="shortcut icon" type="image/png" href="<?= base_url('assets/images/favicon.png'); ?>"/>

    <? $this->load->view('templates/styles', array('appendStyle' =>array(base_url('assets/pages/condominio/lista/css/lista.css')))); ?>

    <!--    <link rel="shortcut icon" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->
    <!--    <link rel="icon" type="image/png" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--" sizes="192x192">-->
    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

</head>
<body>
<? $this->load->view('templates/modais-cliente'); ?>

<div class="visible-sm visible-xs menu-mobile-1">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= base_url_filial('home'); ?>"><img src="<?=base_url_filial('assets/images/logo.png', false); ?>" alt="logo-imobiliaria" class="logo-imobiliaria"></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-mobile">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle pesquisa-toggle" data-toggle="collapse" data-target="#pesquisa-mobile">
                    <span class="fa fa-search"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="menu-mobile">
                <ul class="nav navbar-nav">
                    <li><a href="<?=base_url_filial('quem-somos')?>">QUEM SOMOS</a></li>
                    <li><a href="<?=base_url_filial('imovel/pesquisar')?>">IMÓVEIS</a></li>
                    <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                    <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                    <? if($this->session->has_userdata('usuario')) : ?>
                        <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                    <? else : ?>
                        <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                    <?endif; ?>
                </ul>
            </div>
            <? $this->load->view('templates/pesquisa-mobile-1');?>
        </div>
    </nav>
</div>

<div class="visible-md visible-lg menu-desktop">
    <div class="container topo-menu-2">
        <div class="col-md-3 logo">
            <a href="<?=base_url_filial('home'); ?>">
                <img src="<?= base_url_filial('assets/images/logo.png', false); ?>" alt="logo" class="img-responsive img-imobiliaria">
            </a>
        </div>
        <div class="col-md-6 contato">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <p><?= $_SESSION['filial']['telefone_1']; ?><br><?= $_SESSION['filial']['telefone_2']; ?></p>
            <i class="fa fa-map-marker" aria-hidden="true"></i>
            <p><?= $_SESSION['filial']['endereco']; ?><br> <?= $_SESSION['filial']['cidade']; ?> - <?= strtoupper($_SESSION['filial']['estado']); ?></p>
        </div>

        <div class="col-md-3 sociais">
            <div class="row">
                <span>FIQUE POR PERTO</span>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <? if(strlen($_SESSION['filial']['youtube']) > 0) : ?>
                <li>
                    <a href="<?= $_SESSION['filial']['youtube']; ?>"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                <li>
                    <? endif; ?>
                    <? if(strlen($_SESSION['filial']['instagram']) > 0) : ?>
                <li>
                    <a href="<?= $_SESSION['filial']['instagram']; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </li>
            <? endif; ?>
                <? if(strlen($_SESSION['filial']['twitter']) > 0) : ?>
                    <li>
                        <a href="<?= $_SESSION['filial']['twitter']; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>
                <? endif; ?>
                <? if(strlen($_SESSION['filial']['facebook']) > 0) : ?>
                    <li>
                        <a href="<?= $_SESSION['filial']['facebook']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>
                <? endif; ?>
            </ul>
        </div>
    </div>
    <div class="col-md-12 menu-2">
        <div class="container">
            <div class="col-md-9 menu-items">
                <div class="row">
                    <ul class="nav navbar-nav pull-left">
                        <li><a href="<?=base_url('quem-somos')?>">QUEM SOMOS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar')?>">IMÓVEIS</a></li>
                        <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>

                        <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                        <? if($this->session->has_userdata('usuario')) : ?>
                            <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                        <? else : ?>
                            <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                        <?endif; ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 pesquisa-imovel">
                <form class="row" action="<?= base_url_filial('imovel/pesquisar'); ?>" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" name="id" placeholder="CÓDIGO DO IMÓVEL">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>
<? require_once MODULESPATH . 'simples/helpers/texto_helper.php'; ?>

<div class="container lista-condominios-1">
    <div class="row">
        <div class="col-md-12 col-xs-12 condominios">
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    <p class="titulo">Empreendimentos</p>
                </div>
                <div class="col-md-4 col-xs-12 filtro-tipo">
                    <label for="filtro">Filtro:</label>
                    <select name="filtro_empreendimentos" id="ordenar_condominios" class="selectpicker" onchange="mostrarCondominios(); ">
                        <option value="1">Todos</option>
                        <option value="2">Lançamentos</option>
                        <option value="3">Fechados</option>
                    </select>
                </div>
            </div>
            <? foreach ($condominios as $condominio) : ?>
                <div class="col-md-4 condominio <? if($condominio->fechado == 1): echo 'fechado'; endif; ?> <? if($condominio->lancamento == 1): echo 'lancamento'; endif; ?>">
                    <div class="row">
                        <a href="<?= base_url_filial('condominio?id='.$condominio->id )?>">
                            <div class="col-md-12 img-condominio">
                                <div class="img" style="background-image: url(<?= $_SESSION['filial']['fotos_condominios'] . $condominio->foto; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>"></div>
                                <? if ($condominio->lancamento == 1) :?>
                                    <div class="ribbon lancamento"><span class="finalidade">Lançamento</span></div>
                                <?endif; ?>
                            </div>
                            <div class="col-md-12 detalhes-imovel">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <span class="nome-condominio pull-left"><?= $condominio->nome; ?></span>
                                    </div>
                                </div>
                                <span class="localizacao-condominio text-left"><?= $condominio->cidade; ?>/<?= $condominio->estado; ?></span>
                            </div>
                        </a>
                    </div>
                </div>
            <?endforeach; ?>
        </div>
    </div>
</div><div class="col-md-12 footer-1">
    <hr class="linha-topo">
    <div class="container ">
        <div class="row">
            <div class="col-md-2 visible-lg visible-md">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo" class="img-responsive">
            </div>
            <div class="col-md-3 col-xs-12 endereco">
                <span>Endereço</span>
                <p><?= $_SESSION['filial']['endereco']; ?><br><?= $_SESSION['filial']['cidade']; ?> - <?= strtoupper($_SESSION['filial']['estado']); ?></p>
            </div>
            <div class="col-md-3 col-xs-12 dados-contato">
                <span>Contato</span>
                <p><?= $_SESSION['filial']['email_padrao']; ?><br>+55 <?= $_SESSION['filial']['telefone_1']; ?></p>
            </div>
            <div class="logo-mobile col-xs-12 visible-sm visible-xs">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo" class="img-responsive">
            </div>
            <div class="col-md-4 facebook visible-lg visible-md">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fopencia&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="100%" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
    </div>
    <hr class="linha-baixo">
    <span class="center-block visible-lg visible-md"><a href="http://www.simplesimob.com.br/"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a> Desenvolvido por Simples Imob, todos os direitos reservados.</span>
    <span class="center-block visible-sm visible-xs"><a href="http://www.simplesimob.com.br/"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a></span>
</div>
</body>
<footer>
    <? $this->load->view('templates/scripts', array('appendScripts' =>array(base_url('assets/pages/condominio/lista/js/lista.js')))); ?>
</footer>
</html>
