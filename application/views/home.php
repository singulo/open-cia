<? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>

<!doctype html>
<html lang="pt-BR">

<head>

    <title><?= $_SESSION['filial']['nome']; ?></title>
    <meta name="discription" content=""> <!--- Discrição do site !-->
    <meta name="keywords" content=""> <!--- palavras chave !-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <link rel="shortcut icon" type="image/png" href="<?= base_url('assets/images/favicon.png'); ?>"/>

    <? $this->load->view('templates/styles', array('appendStyle' =>array(base_url('assets/pages/home/css/home.css')))); ?>

    <meta property="og:title" content="<?= $_SESSION['filial']['nome']; ?>">
    <meta property="og:site_name" content="<?= $_SESSION['filial']['nome']; ?>">
    <meta property="og:url" content="<?= $_SESSION['filial']['link']; ?>">
    <meta property="og:type" content="website">
    <meta property="og:image" content="<?= base_url('assets/images/logo-facebook.jpg'); ?>">
    <meta property="og:description" content="Reconhecida por atuar de forma marcante em todo litoral gaúcho, a Open tem o papel de impor um novo conceito de gestão e atendimento ao cliente, trazendo o principio da excelência como seu alicerce. Oferecendo uma experiência de compra ou venda acompanhada de um especialista altamente qualificado para atender as necessidades do publico mais exigente.">

    <!--    <link rel="shortcut icon" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->
    <!--    <link rel="icon" type="image/png" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--" sizes="192x192">-->
    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

</head>
<body class="pagina-home">
<? $this->load->view('templates/modais-cliente'); ?>

<div class="visible-sm visible-xs menu-mobile-1">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= base_url_filial('home'); ?>"><img src="<?=base_url_filial('assets/images/logo.png', false); ?>" alt="logo-imobiliaria" class="logo-imobiliaria"></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-mobile">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle pesquisa-toggle" data-toggle="collapse" data-target="#pesquisa-mobile">
                    <span class="fa fa-search"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="menu-mobile">
                <ul class="nav navbar-nav">
                    <li><a href="<?=base_url_filial('quem-somos')?>">QUEM SOMOS</a></li>
                    <li><a href="<?=base_url_filial('imovel/pesquisar')?>">IMÓVEIS</a></li>
                    <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                    <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                    <? if($this->session->has_userdata('usuario')) : ?>
                        <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                    <? else : ?>
                        <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                    <?endif; ?>
                </ul>
            </div>
            <? $this->load->view('templates/pesquisa-mobile-1');?>
        </div>
    </nav>
</div>

<div class="visible-md visible-lg menu-desktop">
    <div class="container topo-menu-2">
        <div class="col-md-3 logo">
            <a href="<?=base_url_filial('home'); ?>">
                <img src="<?= base_url_filial('assets/images/logo.png', false); ?>" alt="logo" class="img-responsive img-imobiliaria">
            </a>
        </div>
        <div class="col-md-6 contato">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <p <? if(strlen($_SESSION['filial']['telefone_2']) == 0) echo 'style="vertical-align: text-bottom;"';?>>
                <?= $_SESSION['filial']['telefone_1']; ?>
                <? if(strlen($_SESSION['filial']['telefone_2']) > 0) :?>
                    <br>
                    <?= $_SESSION['filial']['telefone_2']; ?>
                <? endif; ?>
            </p>
            <i class="fa fa-map-marker" aria-hidden="true"></i>
            <p><?= $_SESSION['filial']['endereco']; ?><br> <?= $_SESSION['filial']['cidade']; ?> - <?= strtoupper($_SESSION['filial']['estado']); ?></p>
        </div>

        <div class="col-md-3 sociais">
            <div class="row">
                <span>FIQUE POR PERTO</span>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <? if(strlen($_SESSION['filial']['youtube']) > 0) : ?>
                <li>
                    <a href="<?= $_SESSION['filial']['youtube']; ?>"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                <li>
                    <? endif; ?>
                    <? if(strlen($_SESSION['filial']['instagram']) > 0) : ?>
                <li>
                    <a href="<?= $_SESSION['filial']['instagram']; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </li>
            <? endif; ?>
                <? if(strlen($_SESSION['filial']['twitter']) > 0) : ?>
                    <li>
                        <a href="<?= $_SESSION['filial']['twitter']; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>
                <? endif; ?>
                <? if(strlen($_SESSION['filial']['facebook']) > 0) : ?>
                    <li>
                        <a href="<?= $_SESSION['filial']['facebook']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>
                <? endif; ?>
            </ul>
        </div>
    </div>
    <div class="col-md-12 menu-2">
        <div class="container">
            <div class="col-md-9 menu-items">
                <div class="row">
                    <ul class="nav navbar-nav pull-left">
                        <li><a href="<?=base_url('quem-somos')?>">QUEM SOMOS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar')?>">IMÓVEIS</a></li>
                        <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>

                        <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                        <? if($this->session->has_userdata('usuario')) : ?>
                            <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                        <? else : ?>
                            <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                        <?endif; ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 pesquisa-imovel">
                <form class="row" action="<?= base_url_filial('imovel/pesquisar'); ?>" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" name="id" placeholder="CÓDIGO DO IMÓVEL">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><div class="banner-1 visible-md visible-lg">
    <div class="banner">
        <? foreach($_SESSION['filial']['banners'] as $banner) : ?>
            <div class="item" style="background-image: url(<?= $_SESSION['filial']['banners_uri'] . $banner->id . '.jpg'; ?>)">
                <? if (isset($banner->imovel)) : ?>
                    <div class="col-md-12 background-banner"></div>
                    <div class="container">
                        <div class="col-md-8 titulo-banner">
                            <div class="col-md-12">
                                <small><?= $banner->mensagem; ?></small>
                                <br>
                                <span><?= $_SESSION['filial']['tipos_imoveis'][$banner->imovel->id_tipo]->tipo; ?></span>
                            </div>
                            <div class="col-md-3 detalhes">
                                <a href="<?= base_url_filial('imovel?id=' . $banner->imovel->id); ?>">
                                    <span>Veja mais</span>
                                </a>
                            </div>
                        </div>
                    </div>
                <?else :?>
                    <div class="col-md-12 background-banner"></div>
                    <div class="container">
                        <div class="col-md-8 titulo-banner">
                            <div class="col-md-12">
                                <span><?= $banner->mensagem; ?></span>
                            </div>
                        </div>
                    </div>
                <?endif; ?>
            </div>
        <?endforeach; ?>
    </div>
    <div class="container pesquisa-banner visible-md visible-lg">
        <? require_once MODULESPATH . 'simples/helpers/form_values_helper.php'; ?>
        <div>
            <div class="col-md-offset-8 col-md-4 form-pesquisa-banner-1">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-filtro" action="<?= base_url_filial('imovel/pesquisar'); ?>" id="form-pesquisar">
                            <div class="col-md-12">
                                <h3>PESQUISA DE IMÓVEL</h3>
                            </div>
                            <div class="col-md-12 filtro-finalidade">
                                <div class="form-group">
                                    <? foreach( Finalidades::getConstants() as $finalidade => $valor ) : ?>
                                        <label>
                                            <input type="checkbox" name="finalidade" value="<?= $valor; ?>"> <?= $finalidade; ?>
                                        </label>
                                    <? endforeach; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="selectpicker" name="id_tipo" multiple data-width="100%" title="Tipo Imóvel">
                                        <? foreach($_SESSION['filial']['tipos_imoveis'] as $tipo) : ?>
                                            <option value="<?= $tipo->id; ?>"><?= $tipo->tipo;?></option>
                                        <? endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="selectpicker" name="cidade" multiple data-width="100%" title="Localização">
                                        <? foreach($_SESSION['filial']['cidades_imoveis'] as $cidade) : ?>
                                            <option value="<?= $cidade->cidade; ?>"><?= $cidade->cidade;?></option>
                                        <? endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="selectpicker" name="dormitorios" multiple data-width="100%" title="Dorm">
                                        <option value="">-</option>
                                        <? for ($i = 0; $i <= $this->config->item('pesquisa')['dormitorios']; $i++) : ?>
                                            <option value="<?= $i; ?>"><?= $i; ?><? if($i == $this->config->item('pesquisa')['dormitorios']) echo '+'; ?> </option>
                                        <? endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="selectpicker" name="suites" multiple data-width="100%" title="Suítes">
                                        <option value="">-</option>
                                        <? for ($i = 0; $i <= $this->config->item('pesquisa')['suites']; $i++) : ?>
                                            <option value="<?= $i; ?>"><?= $i; ?><? if($i == $this->config->item('pesquisa')['suites']) echo '+'; ?> </option>
                                        <? endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="selectpicker" name="garagem" multiple data-width="100%" title="Vagas">
                                        <option value="">-</option>
                                        <? for ($i = 0; $i <= $this->config->item('pesquisa')['vagas']; $i++) : ?>
                                            <option value="<?= $i; ?>"><?= $i; ?><? if($i == $this->config->item('pesquisa')['vagas']) echo '+'; ?> </option>
                                        <? endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <label for="preco_min" style="margin-top: 5px">O quanto pretende investir?</label>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <input type="text" class="form-control dinheiro" name="preco_min" placeholder="Preço mínino">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <input type="text" class="form-control dinheiro" name="preco_max"  id="preco_max" placeholder="Preço máximo">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label for="id">Busque pelo código:</label>
                                    <input type="text" class="form-control" name="id"  placeholder="Pesquisa por código">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn-pesquisar btn-success" onclick="pesquisar(0);" type="submit">Realizar Pesquisa</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><? require_once MODULESPATH . 'simples/helpers/form_values_helper.php'; ?>
<? require_once MODULESPATH . 'simples/libraries/Finalidades.php'; ?>


<? if( ! isset($filtro))
{
    $filtro = new stdClass();
    $filtro->preco_min = 0;
    $filtro->id = "";
    $filtro->preco_max = 3000000;
    $filtro->metragem_max = 300;
    $filtro->metragem_min = 0;
    $filtro->id_tipo = array();
    $filtro->id_condominio = array();
    $filtro->cidade = array();
    $filtro->finalidade = array();
    $filtro->dormitorios = NULL;
    $filtro->garagem = NULL;
    $filtro->banheiros = NULL;
    $filtro->suites = NULL;
}
?>

<div class="pesquisa-avancada-1">
    <div class="container hidden-xs hidden-sm">
        <div class="row form-pesquisa-avancada">
            <div class="col-md-6 titulo-pesquisa">
                <div class="col-md-12">
                    <div class="row">
                        <h1 class="pull-left">Encontre aqui seu imóvel</h1>
                    </div>
                </div>
                <hr>
            </div>
            <form class="form-filtro" id="filtro-imovel" action="<?= base_url_filial('imovel/pesquisar'); ?>">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-md-2 ">
                            <div class="form-group">
                                <label for="finalidade">Finalidade</label>
                                <select class="selectpicker" name="finalidade" multiple data-width="100%" title="Selecione">
                                    <? foreach( Finalidades::getConstants() as $finalidade => $valor ) : ?>
                                        <option value="<?= $valor;?>" <? if(select_value($valor, $filtro->finalidade)) echo 'selected'; ?>><?= $finalidade; ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3" data-width="100%">
                            <div class="form-group">
                                <label for="tipo">Tipo</label>
                                <select class="selectpicker" name="id_tipo" multiple data-width="100%" title="Selecione">
                                    <? foreach($_SESSION['filial']['tipos_imoveis'] as $tipo) : ?>
                                        <option value="<?= $tipo->id; ?>" <? if(select_value($tipo->id, $filtro->id_tipo)) echo 'selected'; ?>><?= $tipo->tipo;?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="cidade">Localização</label>
                                <select class="selectpicker" name="cidade" multiple data-width="100%" title="Selecione">
                                    <? foreach($_SESSION['filial']['cidades_imoveis'] as $cidade) : ?>
                                        <option value="<?= $cidade->cidade; ?>" <? if(select_value($cidade->cidade, $filtro->cidade)) echo 'selected'; ?> ><?= $cidade->cidade;?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group ">
                                <label for="dormitorios">Dormitórios</label>
                                <select name="dormitorios" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= $this->config->item('pesquisa')['dormitorios']; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->dormitorios) && $i == $filtro->dormitorios) echo 'selected'; ?>><?= $i; ?> <? if($i == $this->config->item('pesquisa')['dormitorios']) echo '+'; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group ">
                                <label for="banheiros">Banheiros</label>
                                <select name="banheiros" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= 10; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->banheiros) && $i == $filtro->banheiros) echo 'selected'; ?>><?= $i; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group ">
                                <label for="suites">Suítes</label>
                                <select name="suites" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= $this->config->item('pesquisa')['suites']; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->suites) && $i == $filtro->suites) echo 'selected'; ?>><?= $i; ?> <? if($i == $this->config->item('pesquisa')['suites']) echo '+'; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group ">
                                <label for="garagem">Vagas</label>
                                <select name="garagem" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= $this->config->item('pesquisa')['vagas']; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->garagem) && $i == $filtro->garagem) echo 'selected'; ?>><?= $i; ?> <? if($i == $this->config->item('pesquisa')['vagas']) echo '+'; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3 slider-valores">
                            <div class="form-group">
                                <? $filtro_valor_min = array('valor' => $filtro->preco_min, 'formatado' => number_format((int)$filtro->preco_min, 2, ',', '.')); ?>
                                <? $filtro_valor_max = array('valor' => $filtro->preco_max, 'formatado' => number_format((int)$filtro->preco_max, 2, ',', '.')); ?>
                                <input type="hidden" name="preco_min" value="<?= $filtro_valor_min['valor']; ?>">
                                <input type="hidden" name="preco_max" value="<?= $filtro_valor_max['valor']; ?>">
                                <div class="legendas">
                                    <span>Valor</span>
                                </div>
                                <input type="text" class="slider-filtro" data-slider-tooltip="hide" data-slider-min="0" data-slider-max="3000000" data-slider-step="50000" data-slider-value="[<?= $filtro_valor_min['valor']; ?>, <?= $filtro_valor_max['valor']; ?>]"/>
                                <div class="filtro-valores">
                                    <span class="pull-left"><small>R$</small> <?= $filtro_valor_min['formatado']; ?> a</span>

                                    <!--                                <span class="pull-left">Minimo</span>-->
                                    <!--                                <span class="pull-right">Máximo </span>-->
                                    <span class="pull-right"><small>R$</small><?= $filtro_valor_max['formatado']; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 slider-valores">
                            <div class="form-group ">
                                <? $filtro_metragem_min= array('valor'  => $filtro->metragem_min ); ?>
                                <? $filtro_metragem_max = array('valor' => $filtro->metragem_max ); ?>
                                <input type="hidden" name="metragem_min" value="<?= $filtro_metragem_min['valor']; ?>">
                                <input type="hidden" name="metragem_max" value="<?= $filtro_metragem_max['valor']; ?>">
                                <div class="legendas">
                                    <span class="pull-left">Metragem</span>
                                </div>
                                <input type="text" class="slider-metragem" data-slider-tooltip="hide" data-slider-min="0" data-slider-max="300" data-slider-step="10" data-slider-value="[<?= $filtro_metragem_min['valor']; ?>,<?= $filtro_metragem_max['valor']; ?>]"/>
                                <div class="filtro-metragem">
                                    <span class="pull-left"><?= $filtro_metragem_min['valor']; ?>m² a</span>
                                    <span class="pull-right"><?= $filtro_valor_max['valor']; ?>m²</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="complementos">Condomínios</label>
                                <select class="selectpicker" name="id_condominio" multiple data-width="100%" title="Selecione">
                                    <? foreach ($_SESSION['filial']['condominios'] as $condominio) : ?>
                                        <option value="<?= $condominio->id; ?>"  data-subtext="<?= $condominio->cidade; ?>" <? if(select_value($condominio->id, $filtro->id_condominio)) echo 'selected'; ?> ><?= $condominio->nome;?></option>
                                    <?endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group ">
                                <label for="id">Código do imóvel</label>
                                <input type="text" class="form-control codigo-imovel" name="id" value="<?= $filtro->id; ?>" placeholder="Digite o código">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <button type="button" onclick="pesquisar(0);" class="btn-success btn-pesquisar"><i class="fa fa-search"></i> Realizar Pesquisa</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    var filtro = <?= json_encode($filtro); ?>;
</script><? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>
<? require_once MODULESPATH . 'simples/libraries/Finalidades.php'; ?>

<div class="destaques-1">
    <div class="container">
        <div class="row">
            <div class="col-md-3 linha">
                <hr>
            </div>
            <div class="col-md-6 titulo-destaque">
                <span >Confira alguns de nossos destaques</span>
            </div>
            <div class="col-md-3 linha">
                <hr>
            </div>
            <? require_once MODULESPATH . 'simples/helpers/texto_helper.php'; ?>
            <? $count = 1; $total_destaques = count($destaques); ?>
            <? foreach($destaques as $imovel) : ?>
                <div class="col-md-4 previa-imovel">
                    <a href="<?= imovel_url($imovel); ?>">
                        <div class="col-md-12 img-imovel" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->foto; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>)"></div>
                        <div class="col-md-12 detalhes-imovel">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <span class="imovel-tipo pull-left"><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?></span>
                                        <span class="valor-imovel"><?= format_valor_miniatura($imovel, 'R$ '); ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <? $detalhes = [];

                                        if($imovel->dormitorios > 0)
                                            $detalhes[] = $imovel->dormitorios . ' dorm.';
                                        if($imovel->area_total > 0)
                                            $detalhes[] = $imovel->area_total . 'm²';
                                        if($imovel->garagem > 0)
                                            $detalhes[] =  $imovel->garagem . ' ' . texto_para_plural_se_necessario($imovel->garagem, 'vaga');
                                        if($imovel->suites > 0)
                                            $detalhes[] = $imovel->suites . ' ' . texto_para_plural_se_necessario($imovel->suites, 'suíte');
                                        ?>
                                        <span class="detalhes pull-left"><?= join(' | ', $detalhes); ?></span>
                                        <span class="codigo pull-right">cód: <?= $imovel->id; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <? if(($count % 3) == 0 && $count != $total_destaques ) :?>

                    <div class="col-md-12 hidden-sm hidden-xs" style="height: 30px; margin-bottom: 16px;">
                        <hr style="width: 964px;">
                    </div>

                <?endif; ?>
                <?$count++; ?>
            <? endforeach; ?>
        </div>
    </div>
</div><div class="container pesquisa-rapida-1">
    <div class="row">
        <div class="col-md-5">
            <a href="http://www.opencia.com.br/imovel/pesquisar?id_tipo=25&id_tipo=24&id_tipo=14&id_tipo=26&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=">
                <div class="busca-rapida">
                    <div class="col-md-12 tipo-destaque" style="background-image: url(http://www.brandaliseimoveis.com.br//vista.imobi/fotos/323179/ia0j3p_3231795638e703d4eec.jpg), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>);">
                        <div class="row background">
                            <span>Casas em Condomínios</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="http://www.opencia.com.br/imovel/pesquisar?id_tipo=1&id_tipo=3&id_tipo=13&id_tipo=2&id_tipo=9&id_tipo=17&id_tipo=8&id_tipo=11&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=300000">
                <div class="busca-rapida">
                    <div class="col-md-12 tipo-destaque" style="background-image: url(http://www.mrv.com.br/fontedasaguas/lib/imgs/alta/fachada.jpg), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>);">
                        <div class="row background">
                            <span>Apartamentos</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a href="http://www.opencia.com.br/imovel/pesquisar?id_tipo=6&id_tipo=7&id_tipo=4&id_tipo=12&id_tipo=15&id_tipo=27&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&met">
                <div class="busca-rapida">
                    <div class="col-md-12 tipo-destaque" style="background-image: url(http://decorandocasas.com.br/wp-content/uploads/2014/05/projetos-de-sobrados-com-3-quartos-4.jpg), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>);">
                        <div class="row background">
                            <span>Casas</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div><div class="col-md-12 condominios-1">
    <div class="container">
        <div class="row">
            <div class="col-md-3 titulo">
                <span>Condomínios </span>
            </div>
            <div class="col-md-2">
                <hr>
            </div>
            <div class="col-md-12 logos-condominios">
                <?foreach ($_SESSION['filial']['condominios'] as $condominio)  :?>
                    <div class="item">
                        <a href="<?= base_url_filial('condominio?id='.$condominio->id )?>">
                            <div class="imagem" style="background-image: url(<?= $_SESSION['filial']['fotos_condominios'] . $condominio->foto; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>)"></div>
                        </a>
                    </div>
                <?endforeach; ?>
            </div>
        </div>
    </div>
</div><div class="col-md-12 footer-1">
    <hr class="linha-topo">
    <div class="container ">
        <div class="row">
            <div class="col-md-2 visible-lg visible-md">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo" class="img-responsive">
            </div>
            <div class="col-md-3 col-xs-12 endereco">
                <span>Endereço</span>
                <p><?= $_SESSION['filial']['endereco']; ?><br><?= $_SESSION['filial']['cidade']; ?> - <?= strtoupper($_SESSION['filial']['estado']); ?></p>
            </div>
            <div class="col-md-3 col-xs-12 dados-contato">
                <span>Contato</span>
                <p><?= $_SESSION['filial']['email_padrao']; ?><br>+55 <?= $_SESSION['filial']['telefone_1']; ?></p>
            </div>
            <div class="logo-mobile col-xs-12 visible-sm visible-xs">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo" class="img-responsive">
            </div>
            <div class="col-md-4 facebook visible-lg visible-md">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fopencia&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="100%" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
    </div>
    <hr class="linha-baixo">
    <span class="center-block visible-lg visible-md"><a href="http://www.simplesimob.com.br/"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a> Desenvolvido por Simples Imob, todos os direitos reservados.</span>
    <span class="center-block visible-sm visible-xs"><a href="http://www.simplesimob.com.br/"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a></span>
</div>
</body>
<footer>
    <? $this->load->view('templates/scripts', array('appendScripts' =>array(base_url('assets/pages/home/js/home.js')))); ?>
</footer>
</html>
