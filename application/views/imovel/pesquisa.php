<? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>

<!doctype html>
<html lang="pt-BR">

<head>

    <title><?= $_SESSION['filial']['nome']; ?></title>
    <meta name="discription" content=""> <!--- Discrição do site !-->
    <meta name="keywords" content=""> <!--- palavras chave !-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <link rel="shortcut icon" type="image/png" href="<?= base_url('assets/images/favicon.png'); ?>"/>

    <? $this->load->view('templates/styles', array('appendStyle' =>array(base_url('assets/pages/imovel/pesquisa/css/pesquisa.css')))); ?>

    <!--    <link rel="shortcut icon" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->
    <!--    <link rel="icon" type="image/png" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--" sizes="192x192">-->
    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

</head>
<body>
<? $this->load->view('templates/modais-cliente'); ?>

<div class="visible-sm visible-xs menu-mobile-1">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= base_url_filial('home'); ?>"><img src="<?=base_url_filial('assets/images/logo.png', false); ?>" alt="logo-imobiliaria" class="logo-imobiliaria"></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-mobile">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle pesquisa-toggle" data-toggle="collapse" data-target="#pesquisa-mobile">
                    <span class="fa fa-search"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="menu-mobile">
                <ul class="nav navbar-nav">
                    <li><a href="<?=base_url_filial('quem-somos')?>">QUEM SOMOS</a></li>
                    <li><a href="<?=base_url_filial('imovel/pesquisar')?>">IMÓVEIS</a></li>
                    <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                    <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                    <? if($this->session->has_userdata('usuario')) : ?>
                        <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                    <? else : ?>
                        <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                    <?endif; ?>
                </ul>
            </div>
            <? $this->load->view('templates/pesquisa-mobile-1');?>
        </div>
    </nav>
</div>

<div class="visible-md visible-lg menu-desktop">
    <div class="container topo-menu-2">
        <div class="col-md-3 logo">
            <a href="<?=base_url_filial('home'); ?>">
                <img src="<?= base_url_filial('assets/images/logo.png', false); ?>" alt="logo" class="img-responsive img-imobiliaria">
            </a>
        </div>
        <div class="col-md-6 contato">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <p><?= $_SESSION['filial']['telefone_1']; ?><br><?= $_SESSION['filial']['telefone_2']; ?></p>
            <i class="fa fa-map-marker" aria-hidden="true"></i>
            <p><?= $_SESSION['filial']['endereco']; ?><br> <?= $_SESSION['filial']['cidade']; ?> - <?= strtoupper($_SESSION['filial']['estado']); ?></p>
        </div>

        <div class="col-md-3 sociais">
            <div class="row">
                <span>FIQUE POR PERTO</span>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <? if(strlen($_SESSION['filial']['youtube']) > 0) : ?>
                <li>
                    <a href="<?= $_SESSION['filial']['youtube']; ?>"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                <li>
                    <? endif; ?>
                    <? if(strlen($_SESSION['filial']['instagram']) > 0) : ?>
                <li>
                    <a href="<?= $_SESSION['filial']['instagram']; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </li>
            <? endif; ?>
                <? if(strlen($_SESSION['filial']['twitter']) > 0) : ?>
                    <li>
                        <a href="<?= $_SESSION['filial']['twitter']; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>
                <? endif; ?>
                <? if(strlen($_SESSION['filial']['facebook']) > 0) : ?>
                    <li>
                        <a href="<?= $_SESSION['filial']['facebook']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>
                <? endif; ?>
            </ul>
        </div>
    </div>
    <div class="col-md-12 menu-2">
        <div class="container">
            <div class="col-md-9 menu-items">
                <div class="row">
                    <ul class="nav navbar-nav pull-left">
                        <li><a href="<?=base_url('quem-somos')?>">QUEM SOMOS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar')?>">IMÓVEIS</a></li>
                        <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>

                        <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                        <? if($this->session->has_userdata('usuario')) : ?>
                            <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                        <? else : ?>
                            <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                        <?endif; ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 pesquisa-imovel">
                <form class="row" action="<?= base_url_filial('imovel/pesquisar'); ?>" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" name="id" placeholder="CÓDIGO DO IMÓVEL">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>


<div class="container pesquisa-1">
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <p id="msg-resultados-encontrados" class="titulo-pesquisa text-left">Buscando <strong>imóveis</strong>, aguarde ...</p>
        </div>
        <div class="col-md-6 filtro-valor">
            <label for="filtro">Filtro:</label>
            <select name="filtro_preco" id="ordenar_por" class="selectpicker">
                <option value="valor_finalidade ASC">Valor menor ao maior</option>
                <option value="valor_finalidade DESC">Valor maior ao menor</option>
            </select>
        </div>
        <div class="col-md-12 col-xs-12 imoveis-resultado-pesquisa">
            <div class="row">
                <div class="col-md-4 previa-imovel" style="display: none;">
                    <div class="row">
                        <a class="imovel-url" href="">
                            <div class="col-md-12 img-imovel">
                                <div class="ribbon"><span class="finalidade">VENDA</span></div>
                                <figure>
                                    <img src="<?= base_url_filial('assets/images/loading.gif', false)?>" class="img-responsive" onerror="this.src = '<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>'">
                                </figure>
                            </div>
                            <div class="col-md-12 detalhes-imovel">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <span class="imovel-tipo pull-left">Apartamento</span><span class="valor-imovel">R$135.000,00</span><br>
                                    </div>
                                </div>
                                <span class="detalhes text-left">3 dorm. | 82m² | 1 vagas | 2 suites</span>
                                <span class="codigo pull-right">cód: 123</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-md-12">
        <div class="center-block" style="display: table;">
            <div id="pagination" class="pagination-holder clearfix">
                <div id="light-pagination" class="pagination light-theme simple-pagination">
                </div>
            </div>
        </div>
    </div>
</div><? require_once MODULESPATH . 'simples/helpers/form_values_helper.php'; ?>
<? require_once MODULESPATH . 'simples/libraries/Finalidades.php'; ?>


<? if( ! isset($filtro))
{
    $filtro = new stdClass();
    $filtro->preco_min = 0;
    $filtro->id = "";
    $filtro->preco_max = 3000000;
    $filtro->metragem_max = 300;
    $filtro->metragem_min = 0;
    $filtro->id_tipo = array();
    $filtro->id_condominio = array();
    $filtro->cidade = array();
    $filtro->finalidade = array();
    $filtro->dormitorios = NULL;
    $filtro->garagem = NULL;
    $filtro->banheiros = NULL;
    $filtro->suites = NULL;
}
?>

<div class="pesquisa-avancada-1">
    <div class="container hidden-xs hidden-sm">
        <div class="row form-pesquisa-avancada">
            <div class="col-md-6 titulo-pesquisa">
                <div class="col-md-12">
                    <div class="row">
                        <h1 class="pull-left">Encontre aqui seu imóvel</h1>
                    </div>
                </div>
                <hr>
            </div>
            <form class="form-filtro" id="filtro-imovel" action="<?= base_url_filial('imovel/pesquisar'); ?>">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-md-2 ">
                            <div class="form-group">
                                <label for="finalidade">Finalidade</label>
                                <select class="selectpicker" name="finalidade" multiple data-width="100%" title="Selecione">
                                    <? foreach( Finalidades::getConstants() as $finalidade => $valor ) : ?>
                                        <option value="<?= $valor;?>" <? if(select_value($valor, $filtro->finalidade)) echo 'selected'; ?>><?= $finalidade; ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3" data-width="100%">
                            <div class="form-group">
                                <label for="tipo">Tipo</label>
                                <select class="selectpicker" name="id_tipo" multiple data-width="100%" title="Selecione">
                                    <? foreach($_SESSION['filial']['tipos_imoveis'] as $tipo) : ?>
                                        <option value="<?= $tipo->id; ?>" <? if(select_value($tipo->id, $filtro->id_tipo)) echo 'selected'; ?>><?= $tipo->tipo;?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="cidade">Localização</label>
                                <select class="selectpicker" name="cidade" multiple data-width="100%" title="Selecione">
                                    <? foreach($_SESSION['filial']['cidades_imoveis'] as $cidade) : ?>
                                        <option value="<?= $cidade->cidade; ?>" <? if(select_value($cidade->cidade, $filtro->cidade)) echo 'selected'; ?> ><?= $cidade->cidade;?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group ">
                                <label for="dormitorios">Dormitórios</label>
                                <select name="dormitorios" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= $this->config->item('pesquisa')['dormitorios']; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->dormitorios) && $i == $filtro->dormitorios) echo 'selected'; ?>><?= $i; ?><? if($i == $this->config->item('pesquisa')['dormitorios']) echo '+'; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group ">
                                <label for="banheiros">Banheiros</label>
                                <select name="banheiros" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= 10; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->banheiros) && $i == $filtro->banheiros) echo 'selected'; ?>><?= $i; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group ">
                                <label for="suites">Suítes</label>
                                <select name="suites" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= $this->config->item('pesquisa')['suites']; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->suites) && $i == $filtro->suites) echo 'selected'; ?>><?= $i; ?><? if($i == $this->config->item('pesquisa')['suites']) echo '+'; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group ">
                                <label for="garagem">Vagas</label>
                                <select name="garagem" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= $this->config->item('pesquisa')['vagas']; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->garagem) && $i == $filtro->garagem) echo 'selected'; ?>><?= $i; ?><? if($i == $this->config->item('pesquisa')['vagas']) echo '+'; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3 slider-valores">
                            <div class="form-group">
                                <? $filtro_valor_min = array('valor' => $filtro->preco_min, 'formatado' => number_format((int)$filtro->preco_min, 2, ',', '.')); ?>
                                <? $filtro_valor_max = array('valor' => $filtro->preco_max, 'formatado' => number_format((int)$filtro->preco_max, 2, ',', '.')); ?>
                                <input type="hidden" name="preco_min" value="<?= $filtro_valor_min['valor']; ?>">
                                <input type="hidden" name="preco_max" value="<?= $filtro_valor_max['valor']; ?>">
                                <div class="legendas">
                                    <span>Valor</span>
                                </div>
                                <input type="text" class="slider-filtro" data-slider-tooltip="hide" data-slider-min="0" data-slider-max="3000000" data-slider-step="50000" data-slider-value="[<?= $filtro_valor_min['valor']; ?>, <?= $filtro_valor_max['valor']; ?>]"/>
                                <div class="filtro-valores">
                                    <span class="pull-left"><small>R$</small> <?= $filtro_valor_min['formatado']; ?> a</span>

                                    <!--                                <span class="pull-left">Minimo</span>-->
                                    <!--                                <span class="pull-right">Máximo </span>-->
                                    <span class="pull-right"><small>R$</small><?= $filtro_valor_max['formatado']; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 slider-valores">
                            <div class="form-group ">
                                <? $filtro_metragem_min= array('valor'  => $filtro->metragem_min ); ?>
                                <? $filtro_metragem_max = array('valor' => $filtro->metragem_max ); ?>
                                <input type="hidden" name="metragem_min" value="<?= $filtro_metragem_min['valor']; ?>">
                                <input type="hidden" name="metragem_max" value="<?= $filtro_metragem_max['valor']; ?>">
                                <div class="legendas">
                                    <span class="pull-left">Metragem</span>
                                </div>
                                <input type="text" class="slider-metragem" data-slider-tooltip="hide" data-slider-min="0" data-slider-max="300" data-slider-step="10" data-slider-value="[<?= $filtro_metragem_min['valor']; ?>,<?= $filtro_metragem_max['valor']; ?>]"/>
                                <div class="filtro-metragem">
                                    <span class="pull-left"><?= $filtro_metragem_min['valor']; ?>m² a</span>
                                    <span class="pull-right"><?= $filtro_valor_max['valor']; ?>m²</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="complementos">Condomínios</label>
                                <select class="selectpicker" name="id_condominio" multiple data-width="100%" title="Selecione">
                                    <? foreach ($_SESSION['filial']['condominios'] as $condominio) : ?>
                                        <option value="<?= $condominio->id; ?>"  data-subtext="<?= $condominio->cidade; ?>" <? if(select_value($condominio->id, $filtro->id_condominio)) echo 'selected'; ?> ><?= $condominio->nome;?></option>
                                    <?endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group ">
                                <label for="id">Código do imóvel</label>
                                <input type="text" class="form-control codigo-imovel" name="id" value="<?= $filtro->id; ?>" placeholder="Digite o código">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <button type="button" onclick="pesquisar(0);" class="btn-success btn-pesquisar"><i class="fa fa-search"></i> Realizar Pesquisa</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    var filtro = <?= json_encode($filtro); ?>;
</script><div class="col-md-12 condominios-1">
    <div class="container">
        <div class="row">
            <div class="col-md-3 titulo">
                <span>Condomínios </span>
            </div>
            <div class="col-md-2">
                <hr>
            </div>
            <div class="col-md-12 logos-condominios">
                <?foreach ($_SESSION['filial']['condominios'] as $condominio)  :?>
                    <div class="item">
                        <a href="<?= base_url_filial('condominio?id='.$condominio->id )?>">
                            <div class="imagem" style="background-image: url(<?= $_SESSION['filial']['fotos_condominios'] . $condominio->foto; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>)"></div>
                        </a>
                    </div>
                <?endforeach; ?>
            </div>
        </div>
    </div>
</div><div class="col-md-12 footer-1">
    <hr class="linha-topo">
    <div class="container ">
        <div class="row">
            <div class="col-md-2 visible-lg visible-md">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo" class="img-responsive">
            </div>
            <div class="col-md-3 col-xs-12 endereco">
                <span>Endereço</span>
                <p><?= $_SESSION['filial']['endereco']; ?><br><?= $_SESSION['filial']['cidade']; ?> - <?= strtoupper($_SESSION['filial']['estado']); ?></p>
            </div>
            <div class="col-md-3 col-xs-12 dados-contato">
                <span>Contato</span>
                <p><?= $_SESSION['filial']['email_padrao']; ?><br>+55 <?= $_SESSION['filial']['telefone_1']; ?></p>
            </div>
            <div class="logo-mobile col-xs-12 visible-sm visible-xs">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo" class="img-responsive">
            </div>
            <div class="col-md-4 facebook visible-lg visible-md">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fopencia&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="100%" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
    </div>
    <hr class="linha-baixo">
    <span class="center-block visible-lg visible-md"><a href="http://www.simplesimob.com.br/"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a> Desenvolvido por Simples Imob, todos os direitos reservados.</span>
    <span class="center-block visible-sm visible-xs"><a href="http://www.simplesimob.com.br/"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a></span>
</div></body>
<footer>
    <? $this->load->view('templates/scripts', array('appendScripts' =>array(base_url('assets/pages/imovel/pesquisa/js/pesquisa.js')))); ?>
</footer>
</html>