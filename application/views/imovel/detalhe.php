<? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>
<? require_once MODULESPATH . 'simples/helpers/texto_helper.php'; ?>

<!doctype html>
<html lang="pt-BR">

<head>

    <? $imovel_titulo = $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo . ' ' . ($imovel->dormitorios > 0 ? $imovel->dormitorios . ' ' . texto_para_plural_se_necessario($imovel->dormitorios, 'dormitório') : '') . ' ' . $imovel->cidade; ?>

    <title><?= $imovel_titulo; ?></title>
    <meta name="discription" content=""> <!--- Discrição do site !-->
    <meta name="keywords" content=""> <!--- palavras chave !-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <link rel="shortcut icon" type="image/png" href="<?= base_url('assets/images/favicon.png'); ?>"/>

    <? if (isset($imovel)) : ?>
        <meta property="og:title" content="<?= $imovel_titulo . ' - ' . $_SESSION['filial']['nome']; ?>">
        <meta property="og:site_name" content="<?= $_SESSION['filial']['nome']; ?>">
        <meta property="og:image" content="<?=  $_SESSION['filial']['fotos_imoveis'] . $imovel->fotos_principais[0]->arquivo; ?>">
        <meta property="og:description" content="<?=  substr($imovel->descricao,0,152)."..."?>">
    <?endif;?>

    <? $this->load->view('templates/styles', array('appendStyle' =>array(base_url('assets/pages/imovel/detalhe/css/detalhe.css')))); ?>

    <!--    <link rel="shortcut icon" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->
    <!--    <link rel="icon" type="image/png" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--" sizes="192x192">-->
    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

</head>
<body>
<? $this->load->view('templates/modais-cliente'); ?>

<div class="visible-sm visible-xs menu-mobile-1">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= base_url_filial('home'); ?>"><img src="<?=base_url_filial('assets/images/logo.png', false); ?>" alt="logo imobiliária" title="logo imobiliária" class="logo-imobiliaria"></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-mobile">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle pesquisa-toggle" data-toggle="collapse" data-target="#pesquisa-mobile">
                    <span class="fa fa-search"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="menu-mobile">
                <ul class="nav navbar-nav">
                    <li><a href="<?=base_url_filial('quem-somos')?>">QUEM SOMOS</a></li>
                    <li><a href="<?=base_url_filial('imovel/pesquisar')?>">IMÓVEIS</a></li>
                    <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                    <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                    <? if($this->session->has_userdata('usuario')) : ?>
                        <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                    <? else : ?>
                        <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                    <?endif; ?>
                </ul>
            </div>
            <? $this->load->view('templates/pesquisa-mobile-1');?>
        </div>
    </nav>
</div>

<div class="visible-md visible-lg menu-desktop">
    <div class="container topo-menu-2">
        <div class="col-md-3 logo">
            <a href="<?=base_url_filial('home'); ?>">
                <img src="<?= base_url_filial('assets/images/logo.png', false); ?>" alt="logo imobiliária" title="logo imobiliária" class="img-responsive img-imobiliaria">
            </a>
        </div>
        <div class="col-md-6 contato">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <p><?= $_SESSION['filial']['telefone_1']; ?><br><?= $_SESSION['filial']['telefone_2']; ?></p>
            <i class="fa fa-map-marker" aria-hidden="true"></i>
            <p><?= $_SESSION['filial']['endereco']; ?><br> <?= $_SESSION['filial']['cidade']; ?> - <?= strtoupper($_SESSION['filial']['estado']); ?></p>
        </div>

        <div class="col-md-3 sociais">
            <div class="row">
                <span>FIQUE POR PERTO</span>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <? if(strlen($_SESSION['filial']['youtube']) > 0) : ?>
                <li>
                    <a href="<?= $_SESSION['filial']['youtube']; ?>"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                <li>
                    <? endif; ?>
                    <? if(strlen($_SESSION['filial']['instagram']) > 0) : ?>
                <li>
                    <a href="<?= $_SESSION['filial']['instagram']; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </li>
            <? endif; ?>
                <? if(strlen($_SESSION['filial']['twitter']) > 0) : ?>
                    <li>
                        <a href="<?= $_SESSION['filial']['twitter']; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>
                <? endif; ?>
                <? if(strlen($_SESSION['filial']['facebook']) > 0) : ?>
                    <li>
                        <a href="<?= $_SESSION['filial']['facebook']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>
                <? endif; ?>
            </ul>
        </div>
    </div>
    <div class="col-md-12 menu-2">
        <div class="container">
            <div class="col-md-9 menu-items">
                <div class="row">
                    <ul class="nav navbar-nav pull-left">
                        <li><a href="<?=base_url('quem-somos')?>">QUEM SOMOS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar')?>">IMÓVEIS</a></li>
                        <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>

                        <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                        <? if($this->session->has_userdata('usuario')) : ?>
                            <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                        <? else : ?>
                            <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                        <?endif; ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 pesquisa-imovel">
                <form class="row" action="<?= base_url_filial('imovel/pesquisar'); ?>" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" name="id" placeholder="CÓDIGO DO IMÓVEL">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><div class="detalhes-imovel-1">
    <div class="container">
        <div class="col-md-6">
            <div class="row imagens-imovel">
                <div class="img-destaque">
                    <? if(isset($imovel->fotos_principais[0])) : ?>
                        <a href="<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->fotos_principais[0]->arquivo; ?>" title="<?= $imovel->fotos_principais[0]->legenda; ?>">
                            <img src="<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->fotos_principais[0]->arquivo; ?>" alt="<?= $imovel->fotos_principais[0]->legenda; ?>" title="<?= $imovel->fotos_principais[0]->legenda; ?>" class="img-responsive">
                            <div class="ribbon <?= strtolower(Finalidades::toString($imovel->finalidade)); ?>"><span><?= Finalidades::toString($imovel->finalidade); ?></span></div>
                        </a>
                    <?else :?>
                        <div class="ribbon <?= strtolower(Finalidades::toString($imovel->finalidade)); ?>"><span><?= Finalidades::toString($imovel->finalidade); ?></span></div>
                        <img src="<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>" alt="imóvel sem foto" title="imóvel sem foto">
                    <?endif; ?>
                </div>
                <? if($this->session->has_userdata('usuario')) : ?>
                    <div class="img-previa">
                        <? foreach($imovel->fotos['normais'] as $foto) : ?>
                            <div class="item">
                                <a href="<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?>" title="<?= $foto->legenda; ?>">
                                    <div class="img" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>);">
                                    </div>
                                </a>
                            </div>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
            </div>
            <? if(!$this->session->has_userdata('usuario')) : ?>
                <div class="aviso-login">
                    <div class="row">
                        <? $total_midias = $total_midias_disponiveis->fotos + $total_midias_disponiveis->videos; ?>
                        <div class="hidden-xs hidden-sm">
                            <p class="pull-left"><i class="fa fa-camera" aria-hidden="true"></i> Efetue o login para ver todas as <? if($total_midias > 0) echo '<b>' . $total_midias . '</b> '; ?>mídias deste imóvel.</p>
                            <a href="#modal-login" data-toggle="modal" class="btn btn-success pull-right">Logar</a>
                        </div>
                        <div class="visible-xs visible-sm">
                            <p class="pull-left"><i class="fa fa-camera" aria-hidden="true"></i> Efetue o login para ver todas as <? if($total_midias > 0) echo '<b>' . $total_midias . '</b> '; ?>mídias deste imóvel.</p>
                            <a href="#modal-login" data-toggle="modal" class="btn btn-success col-xs-12">Logar</a>
                        </div>
                    </div>
                </div>
            <? endif; ?>
        </div>
        <div class="col-xs-12 col-md-6 descricao-imovel">
            <div class="col-md-12 tipo-imovel">
                <hr class="hidden-sm hidden-xs">
                <? require_once MODULESPATH . 'simples/helpers/texto_helper.php'; ?>
                <h1><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?><? if($imovel->dormitorios > 0) echo ' ' . $imovel->dormitorios . ' ' . texto_para_plural_se_necessario($imovel->dormitorios, 'dormitório'); ?></h1>
                <hr class="hidden-sm hidden-xs">
                <h3><?=$imovel->cidade; ?> / <?=$imovel->bairro; ?></h3>
            </div>
            <div class="col-md-12 descricao">
                <p>
                    <?=$imovel->descricao; ?>
                </p>
            </div>
            <div class="col-md-12 infra-imovel">
                <? if( $imovel->dormitorios > 0) : ?>
                    <i class="fa fa-bed"><br><span><?=$imovel->dormitorios; ?> dorm. </span></i>
                <? endif; ?>
                <? if( $imovel->suites > 0) : ?>
                    <i class="fa fa-bathtub"><br><span><?=$imovel->suites; ?> suites. </span></i>
                <? endif; ?>
                <? if( $imovel->garagem > 0) : ?>
                    <i class="fa fa-car"><br><span><?=$imovel->garagem; ?> vagas. </span></i>
                <? endif; ?>
                <? if( $imovel->area_total > 0) : ?>
                    <i class="fa fa-arrows-alt"><br><span><?=$imovel->area_total; ?>m². </span></i>
                <? endif; ?>
            </div>
            <div class="col-xs-12">
                <span class="titulo-descicao">DETALHES DO IMÓVEL <small> cód. <?=$imovel->id; ?></small></span>
            </div>
            <div class="col-md-12">
                <div class="col-md-6 valor-imovel no-padding-left">
                    <? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>
                    <span><small>R$</small> <?= format_valor($imovel->valor); ?></span>
                </div>
                <div class="col-md-6 agendar-visita no-padding-right">
                    <div class="row">
                        <button type="button" class="btn-interesse"><i class="fa fa-paper-plane-o"></i> SOLICITAR INFORMAÇÕES</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><? if($this->session->has_userdata('usuario')) : ?>
    <div class="col-md-12 medias-1">
        <div class="container">
            <div class="row">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab-fotos" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-camera-retro"></i><span>Fotos (<?= count($imovel->fotos['normais']);?>)</span></a></li>
                    <? if(count($imovel->fotos['plantas']) > 0) : ?>
                        <li role="presentation"><a href="#tab-plantas" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-pencil"></i><span>Plantas (<?= count($imovel->fotos['plantas']);?>)</span></a></li>
                    <? endif; ?>
                    <? if(count($imovel->videos) > 0) : ?>
                        <li role="presentation"><a href="#tab-videos" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-video-camera"></i><span>Videos (<?= count($imovel->videos); ?>)</span></a></li>
                    <? endif; ?>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="tab-fotos">
                        <div class="col-md-12 conteudo-panel">
                            <div class="row conteudo-medias center-block">
                                <? foreach($imovel->fotos['normais'] as $foto) : ?>
                                    <div class="col-md-2 margin-foto">
                                        <a href="<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?>" class="img-imovel"  title="<?= $foto->legenda; ?>">
                                            <div class="img" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo;?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>"></div>
                                        </a>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <? if(count($imovel->fotos['plantas']) > 0) : ?>
                        <div role="tabpanel" class="tab-pane fade" id="tab-plantas">
                            <div class="col-md-12 conteudo-panel">
                                <div class=" row conteudo-medias ">
                                    <? foreach($imovel->fotos['plantas'] as $foto) : ?>
                                        <div class="col-md-2 margin-foto">
                                            <a href="<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?>" class="img-imovel"  title="<?= $foto->legenda; ?>">
                                                <div class="img" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo;?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>"></div>
                                            </a>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if(count($imovel->videos) > 0) : ?>
                        <div role="tabpanel" class="tab-pane fade" id="tab-videos">
                            <? foreach($imovel->videos as $video) : ?>
                                <div class="col-md-4">
                                    <div class="img-previa-video">
                                        <a class="video-youtube" href="https://www.youtube.com/watch?v=<?= $video->id_youtube; ?>">
                                            <img class="img-responsive" src="//img.youtube.com/vi/<?= $video->id_youtube; ?>/0.jpg"  alt="vídeo <?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?>" title="vídeo <?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?>">
                                            <span class="fa fa-play"></span>
                                        </a>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
<? endif; ?><div class="complementos-imovel-1">
    <div class="container">
        <? if(isset($imovel->complementos) && count($imovel->complementos) > 0) : ?>
            <div class="col-md-6 complementos">
                <p class="titulo">Mais detalhes deste imóvel</p>
                <? foreach($imovel->complementos as $complemento) :?>
                    <div class="col-md-3 col-xs-6">
                        <div class="row text-left">
                            <span class="fa fa-check"></span> <small><em><?= $complemento->complemento; ?></em></small>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        <? endif; ?>
        <? if(isset($imovel->condominio) && count($imovel->condominio->complementos) > 0) : ?>
            <div class="col-xs-12 col-md-6 complementos-condominio">
                <p class="titulo">Infraestrutura do condomínio</p>
                <? foreach($imovel->condominio->complementos as $complemento) :?>
                    <div class="col-md-3 col-xs-6">
                        <div class="row text-left">
                            <span class="fa fa-check"></span> <small><em><?= $complemento->complemento; ?></em></small>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        <? endif; ?>
    </div>
</div><? if(strlen($imovel->lat_lng) > 0) : ?>
    <div class="localizacao-1 visible-md visible-lg">
        <div class="container">
            <span>Localização do imóvel</span>
            <div id="map-canvas" class="mapa" data-lat-lng="<?= $imovel->lat_lng; ?>" data-marker-none="false" data-radius-size="300" data-circle-around="true"></div>
        </div>
    </div>
<? endif; ?><div class="col-md-12 sugestoes-1">
    <div class="container">
        <span class="titulo">Algumas sugestões para você</span><br>
        <? ob_start(); ?>
        <? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>
        <? ob_end_clean(); ?>
        <? for($i = 0; $i <= 2; $i++ ) : ?>
            <? if(isset($imoveis_similares[$i])) :?>
                <div class="col-md-4">
                    <div class="col-md-12 sugestao-imovel" style="background-image: url(<?=$_SESSION['filial']['fotos_imoveis']. $imoveis_similares[$i]->foto; ?>)">
                        <div class="row">
                            <a href="<?= imovel_url($imoveis_similares[$i]); ?>">
                                <? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>
                                <div class="col-md-12 background-sugestao">
                                    <span class="tipo-imovel"><?= $_SESSION['filial']['tipos_imoveis'][$imoveis_similares[$i]->id_tipo]->tipo ?></span><br>
                                    <?
                                    $imovel_attr = [];

                                    if($imoveis_similares[$i]->dormitorios > 0)
                                        $imovel_attr[] = $imoveis_similares[$i]->dormitorios . ' ' . texto_para_plural_se_necessario($imoveis_similares[$i]->dormitorios, 'dormitório');
                                    if($imoveis_similares[$i]->area_util > 0)
                                        $imovel_attr[] = $imoveis_similares[$i]->area_util . 'm²'
                                    ?>
                                    <span class="detalhes"><?= join(' | ', $imovel_attr); ?></span><br>
                                    <hr>
                                    <span><?=format_valor($imoveis_similares[$i]->valor,'R$'); ?></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            <?endif; ?>
        <? endfor; ?>
    </div>
</div><? require_once MODULESPATH . 'simples/helpers/form_values_helper.php'; ?>
<? require_once MODULESPATH . 'simples/libraries/Finalidades.php'; ?>


<? if( ! isset($filtro))
{
    $filtro = new stdClass();
    $filtro->preco_min = 0;
    $filtro->id = "";
    $filtro->preco_max = 3000000;
    $filtro->metragem_max = 300;
    $filtro->metragem_min = 0;
    $filtro->id_tipo = array();
    $filtro->id_condominio = array();
    $filtro->cidade = array();
    $filtro->finalidade = array();
    $filtro->dormitorios = NULL;
    $filtro->garagem = NULL;
    $filtro->banheiros = NULL;
    $filtro->suites = NULL;
}
?>

<div class="pesquisa-avancada-1">
    <div class="container hidden-xs hidden-sm">
        <div class="row form-pesquisa-avancada">
            <div class="col-md-6 titulo-pesquisa">
                <div class="col-md-12">
                    <div class="row">
                        <h2 class="pull-left">Encontre aqui seu imóvel</h2>
                    </div>
                </div>
                <hr>
            </div>
            <form class="form-filtro" id="filtro-imovel" action="<?= base_url_filial('imovel/pesquisar'); ?>">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-md-2 ">
                            <div class="form-group">
                                <label for="finalidade">Finalidade</label>
                                <select class="selectpicker" name="finalidade" multiple data-width="100%" title="Selecione">
                                    <? foreach( Finalidades::getConstants() as $finalidade => $valor ) : ?>
                                        <option value="<?= $valor;?>" <? if(select_value($valor, $filtro->finalidade)) echo 'selected'; ?>><?= $finalidade; ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3" data-width="100%">
                            <div class="form-group">
                                <label for="tipo">Tipo</label>
                                <select class="selectpicker" name="id_tipo" multiple data-width="100%" title="Selecione">
                                    <? foreach($_SESSION['filial']['tipos_imoveis'] as $tipo) : ?>
                                        <option value="<?= $tipo->id; ?>" <? if(select_value($tipo->id, $filtro->id_tipo)) echo 'selected'; ?>><?= $tipo->tipo;?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="cidade">Localização</label>
                                <select class="selectpicker" name="cidade" multiple data-width="100%" title="Selecione">
                                    <? foreach($_SESSION['filial']['cidades_imoveis'] as $cidade) : ?>
                                        <option value="<?= $cidade->cidade; ?>" <? if(select_value($cidade->cidade, $filtro->cidade)) echo 'selected'; ?> ><?= $cidade->cidade;?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group ">
                                <label for="dormitorios">Dormitórios</label>
                                <select name="dormitorios" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= $this->config->item('pesquisa')['dormitorios']; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->dormitorios) && $i == $filtro->dormitorios) echo 'selected'; ?>><?= $i; ?><? if($i == $this->config->item('pesquisa')['dormitorios']) echo '+'; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group ">
                                <label for="banheiros">Banheiros</label>
                                <select name="banheiros" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= 10; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->banheiros) && $i == $filtro->banheiros) echo 'selected'; ?>><?= $i; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group ">
                                <label for="suites">Suítes</label>
                                <select name="suites" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= $this->config->item('pesquisa')['suites']; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->suites) && $i == $filtro->suites) echo 'selected'; ?>><?= $i; ?> <? if($i == $this->config->item('pesquisa')['suites']) echo '+'; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group ">
                                <label for="garagem">Vagas</label>
                                <select name="garagem" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= $this->config->item('pesquisa')['vagas']; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->garagem) && $i == $filtro->garagem) echo 'selected'; ?>><?= $i; ?> <? if($i == $this->config->item('pesquisa')['vagas']) echo '+'; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3 slider-valores">
                            <div class="form-group">
                                <? $filtro_valor_min = array('valor' => $filtro->preco_min, 'formatado' => number_format((int)$filtro->preco_min, 2, ',', '.')); ?>
                                <? $filtro_valor_max = array('valor' => $filtro->preco_max, 'formatado' => number_format((int)$filtro->preco_max, 2, ',', '.')); ?>
                                <input type="hidden" name="preco_min" value="<?= $filtro_valor_min['valor']; ?>">
                                <input type="hidden" name="preco_max" value="<?= $filtro_valor_max['valor']; ?>">
                                <div class="legendas">
                                    <span>Valor</span>
                                </div>
                                <input type="text" class="slider-filtro" data-slider-tooltip="hide" data-slider-min="0" data-slider-max="3000000" data-slider-step="50000" data-slider-value="[<?= $filtro_valor_min['valor']; ?>, <?= $filtro_valor_max['valor']; ?>]"/>
                                <div class="filtro-valores">
                                    <span class="pull-left"><small>R$</small> <?= $filtro_valor_min['formatado']; ?> a</span>

                                    <!--                                <span class="pull-left">Minimo</span>-->
                                    <!--                                <span class="pull-right">Máximo </span>-->
                                    <span class="pull-right"><small>R$</small><?= $filtro_valor_max['formatado']; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 slider-valores">
                            <div class="form-group ">
                                <? $filtro_metragem_min= array('valor'  => $filtro->metragem_min ); ?>
                                <? $filtro_metragem_max = array('valor' => $filtro->metragem_max ); ?>
                                <input type="hidden" name="metragem_min" value="<?= $filtro_metragem_min['valor']; ?>">
                                <input type="hidden" name="metragem_max" value="<?= $filtro_metragem_max['valor']; ?>">
                                <div class="legendas">
                                    <span class="pull-left">Metragem</span>
                                </div>
                                <input type="text" class="slider-metragem" data-slider-tooltip="hide" data-slider-min="0" data-slider-max="300" data-slider-step="10" data-slider-value="[<?= $filtro_metragem_min['valor']; ?>,<?= $filtro_metragem_max['valor']; ?>]"/>
                                <div class="filtro-metragem">
                                    <span class="pull-left"><?= $filtro_metragem_min['valor']; ?>m² a</span>
                                    <span class="pull-right"><?= $filtro_valor_max['valor']; ?>m²</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="complementos">Condomínios</label>
                                <select class="selectpicker" name="id_condominio" multiple data-width="100%" title="Selecione">
                                    <? foreach ($_SESSION['filial']['condominios'] as $condominio) : ?>
                                        <option value="<?= $condominio->id; ?>"  data-subtext="<?= $condominio->cidade; ?>" <? if(select_value($condominio->id, $filtro->id_condominio)) echo 'selected'; ?> ><?= $condominio->nome;?></option>
                                    <?endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group ">
                                <label for="id">Código do imóvel</label>
                                <input type="text" class="form-control codigo-imovel" name="id" value="<?= $filtro->id; ?>" placeholder="Digite o código">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <button type="button" onclick="pesquisar(0);" class="btn-success btn-pesquisar"><i class="fa fa-search"></i> Realizar Pesquisa</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    var filtro = <?= json_encode($filtro); ?>;
</script><div class="footer-1">
    <hr class="linha-topo">
    <div class="container ">
        <div class="row">
            <div class="col-md-2 visible-lg visible-md">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo rodapé" title="logo rodapé" class="img-responsive">
            </div>
            <div class="col-md-3 col-xs-12 endereco">
                <span>Endereço</span>
                <p><?= $_SESSION['filial']['endereco']; ?><br><?= $_SESSION['filial']['cidade']; ?> - <?= strtoupper($_SESSION['filial']['estado']); ?></p>
            </div>
            <div class="col-md-3 col-xs-12 dados-contato">
                <span>Contato</span>
                <p><?= $_SESSION['filial']['email_padrao']; ?><br>+55 <?= $_SESSION['filial']['telefone_1']; ?></p>
            </div>
            <div class="logo-mobile col-xs-12 visible-sm visible-xs">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo rodapé" title="logo rodapé" class="img-responsive">
            </div>
            <div class="col-md-4 facebook visible-lg visible-md">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fopencia&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="100%" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
    </div>
    <hr class="linha-baixo">
    <span class="center-block visible-lg visible-md"><a href="http://www.simplesimob.com.br/" title="SimplesImob - Sistema de Gerenciamento de Imobiliárias"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias" title="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a> Desenvolvido por Simples Imob, todos os direitos reservados.</span>
    <span class="center-block visible-sm visible-xs"><a href="http://www.simplesimob.com.br/" title="SimplesImob - Sistema de Gerenciamento de Imobiliárias"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias" title="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a></span>
</div></body>
<footer>
    <? $this->load->view('templates/scripts', array('appendScripts' =>array(base_url('assets/pages/imovel/detalhe/js/detalhe.js')))); ?>
</footer>
</html>
