<? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>

<!doctype html>
<html lang="pt-BR">

    <head>

        <title><?= $_SESSION['filial']['nome']; ?></title>
        <meta name="discription" content=""> <!--- Discrição do site !-->
        <meta name="keywords" content=""> <!--- palavras chave !-->
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="viewport" content="width=device-width, user-scalable=no" />

        <? $this->load->view('templates/styles', array('appendStyle' =>array(base_url('assets/pages/ /css/ .css')))); ?>

        <!--    <link rel="shortcut icon" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->
        <!--    <link rel="icon" type="image/png" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--" sizes="192x192">-->
        <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

        <link rel="shortcut icon" href="<?= base_url('assets/images/favicon.ico'); ?>">
        <link rel="icon" sizes="16x16 32x32 64x64" href="/favicon.ico">
        <link rel="icon" type="image/png" sizes="196x196" href="<?= base_url('assets/images/favicon-192.png'); ?>">
        <link rel="icon" type="image/png" sizes="160x160" href="<?= base_url('assets/images/favicon-160.png'); ?>">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url('assets/images/favicon-96.png'); ?>">
        <link rel="icon" type="image/png" sizes="64x64" href="<?= base_url('assets/images/favicon-64.png'); ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('assets/images/favicon-32.png'); ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/images/favicon-16.png'); ?>">
        <link rel="apple-touch-icon" href="<?= base_url('assets/images/favicon-57.png'); ?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url('assets/images/favicon-114.png'); ?>>">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url('assets/images/favicon-72.png'); ?>">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url('assets/images/favicon-144.png'); ?>">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url('assets/images/favicon-60.png'); ?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('assets/images/favicon-120.png'); ?>">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('assets/images/favicon-76.png'); ?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('assets/images/favicon-152.png'); ?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/images/favicon-180.png'); ?>">
        <meta name="msapplication-TileColor" content="#FFFFFF">
        <meta name="msapplication-TileImage" content="<?= base_url('assets/images/favicon-144.png'); ?>">
        <!-- ****** favicons do faviconit.com ****** -->

        <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

    </head>
    <body>
        <? $this->load->view('templates/modais-cliente'); ?>


        <!-- COLE AQUI O CÓDIGO GERADO PELO CONSTRUTOR -->

    </body>
    <footer>
        <? $this->load->view('templates/scripts'); ?>
    </footer>
</html>
