<? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>

<!doctype html>
<html lang="pt-BR">

<head>

    <title><?= $_SESSION['filial']['nome']; ?></title>
    <meta name="discription" content=""> <!--- Discrição do site !-->
    <meta name="keywords" content=""> <!--- palavras chave !-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <link rel="shortcut icon" type="image/png" href="<?= base_url('assets/images/favicon.png'); ?>"/>

    <? $this->load->view('templates/styles', array('appendStyle' =>array(base_url('assets/pages/quemsomos/css/quemsomos.css')))); ?>

    <!--    <link rel="shortcut icon" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->
    <!--    <link rel="icon" type="image/png" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--" sizes="192x192">-->
    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

</head>
<body>
<? $this->load->view('templates/modais-cliente'); ?>

<div class="visible-sm visible-xs menu-mobile-1">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= base_url_filial('home'); ?>"><img src="<?=base_url_filial('assets/images/logo.png', false); ?>" alt="logo-imobiliaria" class="logo-imobiliaria"></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-mobile">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle pesquisa-toggle" data-toggle="collapse" data-target="#pesquisa-mobile">
                    <span class="fa fa-search"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="menu-mobile">
                <ul class="nav navbar-nav">
                    <li><a href="<?=base_url_filial('quem-somos')?>">QUEM SOMOS</a></li>
                    <li><a href="<?=base_url_filial('imovel/pesquisar')?>">IMÓVEIS</a></li>
                    <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                    <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                    <? if($this->session->has_userdata('usuario')) : ?>
                        <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                    <? else : ?>
                        <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                    <?endif; ?>
                </ul>
            </div>
            <? $this->load->view('templates/pesquisa-mobile-1');?>
        </div>
    </nav>
</div>

<div class="visible-md visible-lg menu-desktop">
    <div class="container topo-menu-2">
        <div class="col-md-3 logo">
            <a href="<?=base_url_filial('home'); ?>">
                <img src="<?= base_url_filial('assets/images/logo.png', false); ?>" alt="logo" class="img-responsive img-imobiliaria">
            </a>
        </div>
        <div class="col-md-6 contato">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <p><?= $_SESSION['filial']['telefone_1']; ?><br><?= $_SESSION['filial']['telefone_2']; ?></p>
            <i class="fa fa-map-marker" aria-hidden="true"></i>
            <p><?= $_SESSION['filial']['endereco']; ?><br> <?= $_SESSION['filial']['cidade']; ?> - <?= strtoupper($_SESSION['filial']['estado']); ?></p>
        </div>

        <div class="col-md-3 sociais">
            <div class="row">
                <span>FIQUE POR PERTO</span>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <? if(strlen($_SESSION['filial']['youtube']) > 0) : ?>
                <li>
                    <a href="<?= $_SESSION['filial']['youtube']; ?>"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                <li>
                    <? endif; ?>
                    <? if(strlen($_SESSION['filial']['instagram']) > 0) : ?>
                <li>
                    <a href="<?= $_SESSION['filial']['instagram']; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </li>
            <? endif; ?>
                <? if(strlen($_SESSION['filial']['twitter']) > 0) : ?>
                    <li>
                        <a href="<?= $_SESSION['filial']['twitter']; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>
                <? endif; ?>
                <? if(strlen($_SESSION['filial']['facebook']) > 0) : ?>
                    <li>
                        <a href="<?= $_SESSION['filial']['facebook']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>
                <? endif; ?>
            </ul>
        </div>
    </div>
    <div class="col-md-12 menu-2">
        <div class="container">
            <div class="col-md-9 menu-items">
                <div class="row">
                    <ul class="nav navbar-nav pull-left">
                        <li><a href="<?=base_url('quem-somos')?>">QUEM SOMOS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar')?>">IMÓVEIS</a></li>
                        <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>

                        <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                        <? if($this->session->has_userdata('usuario')) : ?>
                            <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                        <? else : ?>
                            <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                        <?endif; ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 pesquisa-imovel">
                <form class="row" action="<?= base_url_filial('imovel/pesquisar'); ?>" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" name="id" placeholder="CÓDIGO DO IMÓVEL">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="realizacoes-1">
    <div class="container">
        <h3 class="text-center">Suas Realizações, nossas realizações</h3>
        <div class="col-md-6 sobre-empresa">
            <div class="row">
                <p>Esta companhia nasce com a premissa de instalar no Litoral, um novo conceito de imobiliária. Reconhecida por atuar de forma marcante em todo litoral gaúcho, a Open tem o papel de impor um novo conceito de gestão e atendimento ao cliente, trazendo o principio da excelência como seu alicerce. Oferecendo uma experiência de compra ou venda acompanhada de um especialista altamente qualificado para atender as necessidades do publico mais exigente. Trabalhamos 365 dias incansavelmente para dar a nossos clientes as informações em primeira mão, buscamos saber sobre novas tendências, lançamentos futuros entre outras oportunidades de investimento.</p>
            </div>
        </div>
        <div class="col-md-6 img">
            <iframe width="100%" height="300" src="https://www.youtube.com/embed/1dDTwTXEH3s" frameborder="0" gesture="media" allowfullscreen></iframe>
            <hr>
        </div>
    </div>
</div>

<div class="col-md-12 destaques-imobiliaria-1">
    <div class="container">
        <div class="row">
            <div class="col-md-4 sobre-empresa">
                <h3>Pioneira</h3>
                <p>Primeira imobiliária no Litoral Gaúcho a trazer o conceito Open House, onde abrimos as portas de alguns imóveis exclusivos, que você cliente e profissional do mercado, poderá conhecer cada m² acompanhado de um corretor especializado.</p>
            </div>
            <div class="col-md-4 sobre-empresa">
                <h3>Olhar treinado</h3>
                <p>Corretores com olhar treinado para encontrar os melhores imóveis e investimentos. Todos desta equipe recebem treinamento constante, motivados pela vontade de entregar um serviço e atendimento ímpar com foco principal em você.</p>
            </div>
            <div class="col-md-4 sobre-empresa">
                <h3>Filosofia</h3>
                <p>Contratamos somente corretores de ponta, profissionais extraordinários, comprometidos com o objetivo comum da equipe. Atuamos de forma sincronizada e organizada priorizando o compartilhamento de informações. Somos uma equipe seleta e trabalhamos em um ambiente familiar e intimista.</p>
            </div>
            <div class="col-md-4 sobre-empresa">
                <h3>Confiança</h3>
                <p>Priorizamos o relacionamento a longo prazo, portanto optamos por um atendimento discreto, sem expor nossos clientes ao mercado, estreitando este elo de confiança com base na ética e transparência.</p>
            </div>
            <div class="col-md-4 sobre-empresa">
                <h3>Imóveis selecionados</h3>
                <p>Atuamos nas praias de Capão da Canoa, Atlântida e Xangri-lá, nossos esforços estão canalizados na informação, acreditamos possuir o maior cadastro de imóveis a venda do litoral, contudo, priorizamos trabalhar com imóveis de boa qualidade até o mais alto luxo encontrado nos condomínios fechados da região.</p>
            </div>
            <div class="col-md-4 sobre-empresa">
                <h3>Respeito</h3>
                <p>A melhor maneira de mostrar respeito a quem nos deposita confiança, é prestarmos uma assessoria eficiente, considerando a valorização do tempo e os recursos de cada cliente.</p>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 sobre-socios-1">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="http://www.opencia.com.br/assets/images/socio1.jpg" onError="this.src=$('#base_url').val() + 'assets/images/corretor-foto-padrao.png';" alt="foto-socio" class="img-responsive">
                <h3>Janquiel Dresch</h3>
                <p class="historia">Com uma visão “Open” do mercado imobiliário, Janquiel Dresch orgulha-se de ser pioneiro em diversos projetos inovadores, que hoje auxiliam corretores e profissionais do mercado a melhor atenderem seu publico. Atuando como profissional desde 2005, percebeu a carência de profissionais qualificados, e desde então vem se destacando pela incansável busca por conhecimento, encontrando em cursos, palestras e livros, soluções e formas criativas de atender seus clientes e ajudar seus colaboradores.</p>
            </div>
            <div class="col-md-6">
                <img src="http://www.opencia.com.br/assets/images/socio2.jpg" onError="this.src=$('#base_url').val() + 'assets/images/corretor-foto-padrao.png';" alt="foto-socio" class="img-responsive">
                <h3>Alexsander Rezer Mainardi</h3>
                <p class="historia">Com 25 anos de experiência profissional, Alexsander Rezer Mainardi está preparado para o maior desafio de sua carreira. A frente desta companhia, usa seu expertise para fazer da Open, a imobiliária referencia em qualidade e excelência no litoral gaúcho. Entende que não vendemos m², e ao entregar um espaço que será chamado de lar, estamos fazendo parte da realização de um sonho ou projeto de vida de uma família, esta é a responsabilidade que nos move.</p>
            </div>
        </div>
    </div>
</div><? if(count($corretores) > 0) :?>
    <div class="col-md-12 corretores-1">
        <div class="container">
            <h3>Realizadores de sonhos</h3>
            <div class="row">
                <? foreach ($corretores as $corretor) : ?>
                    <div class="col-md-3 ">
                        <div class="col-md-12 corretor" style="background-image: url(<?= $_SESSION['filial']['fotos_corretores'] . $corretor->id_corretor .'jpg),'; ?> url(<?= base_url_filial('assets/images/corretor-foto-padrao.png', false); ?>);">
                            <div class="col-md-12 detalhes-corretor" style="opacity: 0;">
                                <span class="nome-corretor"><?= $corretor->nome; ?></span><br>
                                <span><?= $corretor->email; ?></span><br>
                                <span>CRECI: <?= $corretor->creci?></span>
                            </div>
                        </div>
                    </div>
                <?endforeach; ?>
            </div>
        </div>
    </div>
<?endif; ?>
<div class="col-md-12 footer-1">
    <hr class="linha-topo">
    <div class="container ">
        <div class="row">
            <div class="col-md-2 visible-lg visible-md">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo" class="img-responsive">
            </div>
            <div class="col-md-3 col-xs-12 endereco">
                <span>Endereço</span>
                <p><?= $_SESSION['filial']['endereco']; ?><br><?= $_SESSION['filial']['cidade']; ?> - <?= strtoupper($_SESSION['filial']['estado']); ?></p>
            </div>
            <div class="col-md-3 col-xs-12 dados-contato">
                <span>Contato</span>
                <p><?= $_SESSION['filial']['email_padrao']; ?><br>+55 <?= $_SESSION['filial']['telefone_1']; ?></p>
            </div>
            <div class="logo-mobile col-xs-12 visible-sm visible-xs">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo" class="img-responsive">
            </div>
            <div class="col-md-4 facebook visible-lg visible-md">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fopencia&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="100%" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
    </div>
    <hr class="linha-baixo">
    <span class="center-block visible-lg visible-md"><a href="http://www.simplesimob.com.br/"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a> Desenvolvido por Simples Imob, todos os direitos reservados.</span>
    <span class="center-block visible-sm visible-xs"><a href="http://www.simplesimob.com.br/"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a></span>
</div></body>
<footer>
    <? $this->load->view('templates/scripts', array('appendScripts' =>array(base_url('assets/pages//js/.js')))); ?>
</footer>
</html>
