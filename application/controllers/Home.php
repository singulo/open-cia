<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH."../modules/simples/core/Base_Controller.php";

/**
 * @property Imovel_Model $imovel_model
 * @property Condominio_Model $condominio_model
 */
class Home extends Base_Controller {

	public function index()
	{
		$this->load->model('simples/imovel_model');

		$data['destaques'] = $this->imovel_model->destaques(9);

		$this->load->view('home', $data);
	}
}
