<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

class Link_Personalizado_Model extends Base_Model
{
    protected $table = 'tb_link_personalizado';

    public function pelo_link_personalizado($link)
    {
        return $this->obter($link, 'link_personalizado');
    }
}