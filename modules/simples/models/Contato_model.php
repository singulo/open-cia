<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

class Contato_Model extends Base_Model
{
    public $id;
    public $nome;
    public $telefone;
    public $email;
    public $assunto;
    public $mensagem;
    public $criado_em;

    protected $table = 'tb_contato';

    public function novo(Contato_Model $contato)
    {
        return parent::inserir($contato);
    }

}