<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once MODULESPATH . 'simples/libraries/Finalidades.php';

if ( ! function_exists('format_valor'))
{
    function format_valor($valor, $cifrao = false)
    {
		if($valor == 'Consulte')
			return $valor;
		else{
			$valor = number_format($valor, 2, ',', '.');

			if($cifrao != false)
				$valor = $cifrao . $valor;

			return $valor;
		}
	}

	function format_valor_miniatura($imovel, $cifrao = false)
	{
		switch($imovel->finalidade)
		{
			case Finalidades::Venda :
				return format_valor($imovel->valor, $cifrao);
				break;
			case Finalidades::Aluguel :
				return format_valor($imovel->valor_aluguel_mensal, $cifrao);
				break;
			case Finalidades::Temporada :
				return format_valor($imovel->valor_aluguel_diario, $cifrao);
				break;
		}
	}
}
