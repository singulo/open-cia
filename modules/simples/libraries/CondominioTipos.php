<?php

class CondominioTipos
{
    const Vertical = 1;
    const Horizontal = 2;
    const Residencial  = 3;
    const Comercial  = 4;

    public static function toString($val){
        $tmp = new ReflectionClass(get_called_class());
        $a = $tmp->getConstants();
        $b = array_flip($a);

        return ucfirst($b[$val]);
    }

    public static function getConstants()
    {
        $oClass = new ReflectionClass(__CLASS__);
        return $oClass->getConstants();
    }
}