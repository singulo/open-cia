<?php

class ADMNivel
{
    const Adm           = 1;
    const Diretor       = 2;
    const Gerente       = 3;
    const Secretaria    = 4;
    const Agenciador    = 5;
    const Corretor      = 6;

    public static function toString($val){
        $tmp = new ReflectionClass(get_called_class());
        $a = $tmp->getConstants();
        $b = array_flip($a);

        return ucfirst($b[$val]);
    }

    public static function AuthBiggerOrEqualsThan($nivel)
    {
        $CI =& get_instance();

        return $CI->session->userdata('admin')->nivel > $nivel;
    }

    public static function AuthLessThan($nivel)
    {
        $CI =& get_instance();

        return $CI->session->userdata('admin')->nivel < $nivel;
    }

    public static function getConstants()
    {
        $oClass = new ReflectionClass(__CLASS__);
        return $oClass->getConstants();
    }
}